package fr.ulille.iut.pizzaland.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder.In;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import fr.ulille.iut.pizzaland.representation.Ingredient;
import fr.ulille.iut.pizzaland.service.IngredientService;

@RestController
@RequestMapping("ingredients")
public class IngredientController {
    private IngredientService ingredientService;

    @Autowired
    IngredientController(IngredientService service) {
        this.ingredientService = service;
    }

    @GetMapping
    public List<Ingredient> getIngredients() {
        return ingredientService.getAll();
    }

    @GetMapping("{id}")
    public Ingredient getOneIngredient(@PathVariable Long id) {
        Optional<Ingredient> result = ingredientService.getOne(id);

        return result.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "ingredient non trouvé"));
    }

    @PostMapping
    public ResponseEntity<Ingredient> createIngredient(@RequestBody @Valid Ingredient ingredient) {
        Ingredient created = ingredientService.createIngredient(ingredient);
        URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/" + created.getId())
                .build().toUri();
        
            return ResponseEntity.created(location).body(created);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteIngredient(@PathVariable long id) {
        try {
            ingredientService.delete(id);
        }
        catch ( Exception ex ) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "l'ingrédient n'existe pas");
        }
        return ResponseEntity.noContent().build();
    }
}
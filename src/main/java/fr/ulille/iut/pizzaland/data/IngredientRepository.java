package fr.ulille.iut.pizzaland.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.ulille.iut.pizzaland.representation.Ingredient;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

}